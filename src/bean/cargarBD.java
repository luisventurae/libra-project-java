package bean;

import clases.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class cargarBD extends conexion {

    public boolean registrarPC() {

        PreparedStatement pst = null;

        try {
            String consulta = "INSERT INTO `req_actual`(`mac`, `cpu`, `ram`, `os`, `video`, `disco`) "
                    + "VALUES (?, ?, ?, ?, ?, ?); ";
            pst = getConexion().prepareStatement(consulta, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, bean.datatpc.getMac());
            pst.setString(2, bean.datatpc.getCpu());
            pst.setString(3, bean.datatpc.getRam());
            pst.setString(4, bean.datatpc.getOs());
            pst.setString(5, bean.datatpc.getVideo());
            pst.setString(6, bean.datatpc.getDisco());
            pst.executeUpdate();
            ResultSet rs = pst.getGeneratedKeys();
            if (rs.next()) {
                bean.datatpc.setCodpc("" + rs.getInt(1));
            }
            return true;
        } catch (Exception ex) {
            System.out.println("ERROR " + ex);
            ex.printStackTrace();
        } finally {
            try {
                if (getConexion() != null) {
                    getConexion().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("ERROR" + e);
            }
        }
        return false;
    }
}
