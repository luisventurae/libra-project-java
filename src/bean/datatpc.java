package bean;

public class datatpc {

    public static String codpc, mac, cpu, ram, os, video, disco;

    public static String getCodpc() {
        return codpc;
    }

    public static void setCodpc(String codpc) {
        datatpc.codpc = codpc;
    }

    public static String getMac() {
        return mac;
    }

    public static void setMac(String mac) {
        datatpc.mac = mac;
    }

    public static String getCpu() {
        return cpu;
    }

    public static void setCpu(String cpu) {
        datatpc.cpu = cpu;
    }

    public static String getRam() {
        return ram;
    }

    public static void setRam(String ram) {
        datatpc.ram = ram;
    }

    public static String getOs() {
        return os;
    }

    public static void setOs(String os) {
        datatpc.os = os;
    }

    public static String getVideo() {
        return video;
    }

    public static void setVideo(String video) {
        datatpc.video = video;
    }

    public static String getDisco() {
        return disco;
    }

    public static void setDisco(String disco) {
        datatpc.disco = disco;
    }
}
