package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class conexion {

    private String username = "root";
    private String password = "";
    private String host = "localhost";
//    private String port = "80";
    private String database = "bdlibra";
    private String encoding = "useUnicode=true&characterEncoding=utf-8";
    private String classname = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://" + host + "/" + database + "?" + encoding;/*  "jdbc:mysql://127.0.0.1/bdlibra?useUnicode=true&characterEncoding=utf-8","root",""  */
    Connection con;

    public conexion() {
        try {
            Class.forName(classname);
            con = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            System.err.println("ERROR " + e);
        } catch (SQLException e) {
            System.err.println("Error " + e);
        }
    }

    public Connection getConexion() {
        return con;
    }

    public static void main(String[] args) {
        conexion con = new conexion();
    }
}
