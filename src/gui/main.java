package gui;

import bean.cargarBD;
import java.io.File;
import java.lang.management.ManagementFactory;
import javax.swing.JOptionPane;
import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.NetInterfaceConfig;
import org.hyperic.sigar.OperatingSystem;
import org.hyperic.sigar.Sigar;

public class main extends javax.swing.JFrame {

    Sigar s;
    OperatingSystem os;
    NetInterfaceConfig net;

    public main() {
        initComponents();
        cargarData();
    }

    private void cargarData() {
        //Info del Sistema Operativo
        try {
            os = OperatingSystem.getInstance();
            bean.datatpc.setOs(os.getDescription() + " " + os.getVendorName() + " " + os.getVersion() + " " + os.getArch());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error al cargar los datos del S.O.\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        //Info del Procesador
        try {
            s = new Sigar();
            CpuInfo cpu[] = s.getCpuInfoList();
            CpuInfo info = cpu[0];
            bean.datatpc.setCpu(info.getVendor() + "" + info.getModel());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error al cargar los datos del CPU\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        //Info de la MAC
        try {
            net = s.getNetInterfaceConfig(null);
            bean.datatpc.setMac(net.getHwaddr());
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(this, "Error al cargar los datos de la MAC\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("Error al cargar los datos de la MAC... PUEDE DEVERSE A LA FALTA DE CONEXIÓN A UNA RED");
            e.printStackTrace();
        }
        //Info del disco
        try {
            File file = new File("C:");
            long libreC = file.getFreeSpace(); // Espacio libre en disco (Bytes).
            bean.datatpc.setDisco((((libreC / 1024) / 1024) / 1024) + " GB");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error al cargar los datos del disco\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        //Info de la RAM
        try {
            long memorySize = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize();
            bean.datatpc.setRam((((memorySize / 1024) / 1024) / 1024) + " GB");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error al cargar los datos del disco\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }

        cargarBD crg = new cargarBD();
        crg.registrarPC();
        //Mostrar CODIGO LIBRA
        txtidLibra.setText(bean.datatpc.getCodpc());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtidLibra = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Libra Sincronizador");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(34, 34, 34));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Papyrus", 0, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 51));
        jLabel1.setText("L");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 30, -1, -1));

        jLabel2.setFont(new java.awt.Font("Papyrus", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("compara y compra");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 80, -1, -1));

        jLabel3.setFont(new java.awt.Font("Papyrus", 0, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("ibra");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 30, -1, -1));

        txtidLibra.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel1.add(txtidLibra, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, 370, 30));

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Copiar el ID");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 120, -1, -1));

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Info de sincronización");
        jLabel5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 210, -1, -1));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 476, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        String info = "S.O.: " + bean.datatpc.getOs() + "\n"
                + "CPU.: " + bean.datatpc.getCpu() + "\n"
                + "MAC: " + bean.datatpc.getMac() + "\n"
                + "Espacio: " + bean.datatpc.getDisco() + " en Disco C\n"
                + "RAM: " + bean.datatpc.getRam() + " en Disco C\n";
        JOptionPane.showMessageDialog(this, info, "Info asincronizar", JOptionPane.DEFAULT_OPTION);
    }//GEN-LAST:event_jLabel5MouseClicked

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtidLibra;
    // End of variables declaration//GEN-END:variables
}
